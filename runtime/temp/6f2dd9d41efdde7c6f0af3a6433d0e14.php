<?php /*a:3:{s:85:"D:\phpProject\phpstudy_pro\WWW\zhengduimen\application\index\view\index\advisory.html";i:1612590688;s:78:"D:\phpProject\phpstudy_pro\WWW\zhengduimen\application\common\view\header.html";i:1612591914;s:78:"D:\phpProject\phpstudy_pro\WWW\zhengduimen\application\common\view\footer.html";i:1612598462;}*/ ?>
<!DOCTYPE html>
<html class="fly-html-layui fly-html-store">
<head>
    <meta charset="utf-8">
    <title>对门IT</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="referrer" content="never">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="keywords" content="你所要的都在对门，对门有IT想你所想，急你所急">
    <meta name="description" content="你所要的都在对门，对门有IT想你所想，急你所急">
    <link rel="stylesheet" href="/static/x_admin/css/font.css">
    <link rel="stylesheet" href="/static/x_admin/css/store.css">
    <link rel="stylesheet" href="/static/x_admin/lib/layui/css/layui.css">
    <link rel="stylesheet" href="/static/x_admin/css/global2.css">
    <link rel="stylesheet" href="/static/x_admin/css/global1.css">
    <link rel="stylesheet" href="/static/x_admin/css/xadmin.css">
    <link rel="stylesheet" href="/static/x_admin/css/qq_xuanfu.css">
    <link rel="stylesheet" href="/static/current/detail.css">
    <link rel="stylesheet" href="/static/current/normal.css">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
    <script src="/static/x_admin/lib/layui/layui.js"></script>
    <script src="/static/x_admin/js/xadmin.js"></script>
    <script src="/static/x_admin/js/main.js"></script>
</head>
<body>
<div class="layui-header header header-store" style="background-color: #24262F;">
    <div class="layui-container">
        <a class="logo" href="/" style="color: #D0D1D3;font-size: 25px;font-weight: 400">
            对门IT
        </a>
        <div class="layui-form component" lay-filter="LAY-site-header-component"></div>
        <ul class="layui-nav">
            <li class="layui-nav-item layui-hide-xs"><a href="/index.php/index/index/index.html">首页</a>

            </li>
            <!-- <li class="layui-nav-item layui-hide-xs "><a href="/extend/">教程</a></li> -->
            <li class="layui-nav-item layui-hide-xs"><a href="/index.php/index/index/video.html" target="_self">课程视频
                <span class="layui-badge-dot" style="right: 0px;"></span></a></li>
            <li class="layui-nav-item layui-hide-xs"><a href="/index.php/index/index/advisory.html">工单咨询</a>
                <span class="layui-badge-dot" style="right: 0px;"></span></li>
            <li class="layui-nav-item layui-hide-xs">
                <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=1677156127&site=qq&menu=yes">
                    联系客服
                </a>
            </li>
        </ul>
    </div>
</div>


<div class="body_back_color">
    <div class="temp-normal">
        <p class="p_algin">如有意向,请准确填写相关信息,以便客服可以准确的联系到您,相关信息不会泄密,<br>最便捷方式请直接使用右侧客服联系</p>
    </div>
    <div class="temp-normal" id="intention">
        <div class="layui-container">
            <p class="temp-title-cn margin-80"><span></span>快速工单
                <span></span>
            </p>
            <form class="layui-form" method="post">
                <div class="layui-form-item">
                    <div class="layui-form-item">
                        <label for="username" class="layui-form-label">
                            <span class="x-red">*</span>姓名</label>
                        <div class="layui-input-inline">
                            <input type="text" id="username" name="username" required="" lay-verify="required"
                                autocomplete="off" class="layui-input" placeholder="请输入姓名/昵称">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-form-item">
                        <label for="username" class="layui-form-label">
                            <span class="x-red">*</span>电话</label>
                        <div class="layui-input-inline">
                            <input type="text" id="userphone" name="userphone" required="" lay-verify="required"
                                autocomplete="off" class="layui-input" placeholder="请输入电话号码">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-form-item">
                        <label for="username" class="layui-form-label">
                            <span class="x-red">*</span>QQ</label>
                        <div class="layui-input-block">
                            <input type="text" id="userqq" name="userqq" required="" lay-verify="required"
                                autocomplete="off" class="layui-input" placeholder="请输入QQ号码,客服多是通过QQ直接联系">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label"><span class="x-red">*</span>意向课程</label>
                    <div class="layui-input-block">
                        <select name="course" lay-verify="required">
                            <option value=""></option>
                            <option value="0">android</option>
                            <option value="1">flutter</option>
                            <option value="2">php</option>
                            <option value="3">其他</option>
                        </select>
                    </div>
                </div>

                <div class="layui-form-item layui-form-text">
                    <label class="layui-form-label">其他描述</label>
                    <div class="layui-input-block">
                        <textarea name="desc" placeholder="请输入内容" class="layui-textarea"></textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="layui-container">
        <p class="temp-title-cn"><span></span>最新咨询<span></span></p>
    </div>
    <div class="layui-container">
        <ul>
            <?php if(is_array($resIntention) || $resIntention instanceof \think\Collection || $resIntention instanceof \think\Paginator): $i = 0; $__LIST__ = $resIntention;if( count($__LIST__)==0 ) : echo "暂时没有咨询记录" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;if(empty($vo['desc'])): ?>
            <li>网友 <span><?php echo htmlentities($vo['username']); ?></span> 咨询 <span class="layui-btn layui-btn-xs"> <?php echo htmlentities($vo['course']); ?> </span></li>
            <br />
            <?php else: ?>
            <li>网友 <span><?php echo htmlentities($vo['username']); ?></span> 咨询 <span class="layui-btn layui-btn-xs"> <?php echo htmlentities($vo['course']); ?> </span> 他的问题是
                <?php echo htmlentities($vo['desc']); ?></li>
            <br />
            <?php endif; ?>
            <?php endforeach; endif; else: echo "暂时没有咨询记录" ;endif; ?>
        </ul>
    </div>
</div>
<div class="fly-footer" style="background: white;">
    <p><a href="/">对门IT</a>2021 ©<a href="/"><?php echo getServer(); ?></a></p>
    <p><a href="<?php echo getServer(); ?>" target="_blank" title="对门IT">对门IT</a>
        <!-- <a href="<?php echo getServer(); ?>" target="_blank" title="对门IT">对门IT</a> -->
        <a href="http://www.baidu.com" target="_blank" title="百度一下">百度一下</a>
    </p>
</div>
<div class="infoWin">
    <div>
        <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=1677156127&site=qq&menu=yes" style="border-radius: 0px;">
            <img src="http://wpa.qq.com/pa?p=2:1677156127:53" alt="主讲老师" title="主讲老师" style="border-radius: 0px;"/>
        </a>
    </div>
</div>
</body>

</html>
<script>
    layui.use('form', function () {
        var form = layui.form;
        //监听提交
        form.on('submit(formDemo)', function (data) {
            // layer.msg(JSON.stringify(data.field));
            // layui.alert("确定提交?",{ //这个弹窗造成了影响，无法提交数据
            //     skin: 'layui-layer-molv'
            //     ,closeBtn: 1 // 是否显示关闭按钮
            //     ,anim: 1
            //     ,btn: ['确定', '取消'] //按钮
            //     ,icon: 6 // icon
            //     ,yes:function (){
            $.ajax({
                type: "POST"
                , url: "<?php echo getServer(); ?>/index.php/index/index/intention"
                , data: data.field
                , success: function (success) {
                    layer.msg(success.msg);
                    if (success.code === "0") {
                        window.location.reload()
                    }
                }
                , error: function (error) {
                    layer.msg(error.msg);
                }
            })
            //     }
            // })
            return false;
        });
    });
</script>