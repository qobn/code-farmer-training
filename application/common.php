<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
define('LOCALURL','http://www.zhengduimen.io');
define('SERVICE','http://zhengduimen.eatandshow.com');
define('IMGURL','http://zhengduimen.eatandshow.com');
define('ISCESHI',true);
/**
 * 获取服务器地址
 * @return string
 */
function getServer(){
    if (ISCESHI){
        return LOCALURL;
    }else{
        return SERVICE;
    }
}
function getImgUrl(){
    return IMGURL;
}
// 应用公共文件
function getReturnData($data){
    return json($data);
}
