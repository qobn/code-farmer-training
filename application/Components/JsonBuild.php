<?php


namespace app\Components;

class JsonBuild
{
    public static function returnJson($code,$data=array(),$msg,$type=0){
        if (empty($data)){
            $mData = array("code"=>$code,"msg"=>$msg);
        }else{
            $mData = array("code"=>$code,"data"=>$data,"msg"=>$msg);
        }
        return json($mData);
    }
    public static function returnTestJson($code='100',$data=array(),$msg='测试json',$type=0){
        $mData = array("code"=>$code,"data"=>$data,"msg"=>$msg);
        return json($mData);
    }
}