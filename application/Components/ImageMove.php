<?php


namespace app\Components;


class ImageMove
{
    /**
     * @param $sourcefile 源文件
     * @param $dir 目录
     * @param $filename 文件名
     * @return bool
     */
    static function file2dir($sourcefile, $dir,$filename){
        $dest_dir = iconv($dir);
        if(!file_exists($sourcefile)){
            return false;
        }
        if (!file_exists($dir)){
            mkdir($dest_dir);
        }
        return copy($sourcefile, $dest_dir .''. $filename);
    }
}