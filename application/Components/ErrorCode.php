<?php

namespace app\Components;
class ErrorCode
{
    /**
     * code码
     */
    const code_0="0";
    const code_1="1";
    const code_2="2";
    const code_3="3";
    const code_4="4";
    const code_5="5";
    const code_6="6";
    const code_99="99";
    const code_100="100";

    /**
     * code_msg
     */
    const code_account_success = "账号注册成功";
    const code_account_already_has = "账号已被注册";
    const code_account_error = "账号/密码不能为空";
    const code_type_exception = "请求方式异常";
    const code_login_success = "登录成功";
    const code_login_password = "密码错误";
    const code_login_account_empty = "账号不存在";
    const code_send_house_error = "必要参数不存在";
    const code_send_house_success = "发布成功";
    const code_test_msg = "测试";
    const code_find_house_success = "查询成功";
    const code_find_house_empty = "暂无数据";
    const code_find_house_error = "查询失败";
    const code_find_house_params_empty = "必要参数不存在";
    const code_rent_house_order_params_empty = "必要参数不存在";
    const code_rent_house_order_mkdir_error = "图片文件夹创建失败";
    const code_rent_house_order_copy_img = "图片存储失败";
    const code_rent_house_order_success = "免租租房信息发布成功";
    const code_rent_house_order_save_error = "保存失败";
    const code_rent_house_order_already_has = "意向房源已存在,请到我的意向列表查看";
    const code_like_house_order_params_empty = "必要参数不存在";
    const code_like_house_order_undata = "没得数据了";
    const code_like_house_order_success = "查询成功";
    const code_main = "查询成功";
    const code_main_error = "查询失败";
    const code_like_rent_house_update_error = "更新失败";
    const code_like_rent_house_update_success = "点赞成功";
    const code_unlike_rent_house_update_success = "已告知求租者,会继续改进的";
    const code_skill_undata = "技能清单查询失败";
    const code_skill_select_success = "查询成功";
    const code_feedback_empty = "必要参数不存在";
    const code_feedback_already = "反馈已提交,请不要重复提交";
    const code_feedback_success = "反馈成功";
    const code_feedback_error = "反馈失败";
    const code_update_user_empty = "必要参数缺失";
    const code_update_user_no = "该用户未注册";
    const code_update_user_avatar = "图片文件夹创建失败";
    const code_update_user_error = "更新用户信息失败";
    const code_unupdate_user = "数据无改变,不更新";
    const code_update_user_succes = "更新成功";
    const code_find_user_empty = "必要参数缺失";
    const code_find_user_unhas = "当前用户不存在";
    const code_find_user_success = "查询用户成功";
    const code_forgetpswwords_user_undata = "用户信息不存在";
    const code_forgetpswwords_user_reset = "密码重置成功";
    const code_forgetpswwords_user_error = "密码重置异常";
    const code_update_apk_empty = "更新接口异常";
    const code_update_apk_success = "更新接口查询成功";
}