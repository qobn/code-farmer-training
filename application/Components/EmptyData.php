<?php


namespace app\Components;

/**
 * 判空
 * Class EmptyData
 * @package app\Components
 */
class EmptyData
{
    /**
     * 字符串非空判断 增加过滤0
     * @param $data
     * @return bool
     */
    static function emptyData($data){
        if ($data==0||$data=="0"){
            return false;
        }else{
            return empty($data);
        }
    }

    /**
     * 数组 对象非空判断
     * @param $data
     * @return bool
     */
    static function emptyArray($data){
        return empty($data);
    }
}