<?php


/**
 * 为什么用WebSocket?
 *  HTTP的通信只能由客户端发起
 *
 * WebSocket 协议是基于TCP的一种新的网络协议。实现了浏览器与服务器全双工通信——允许服务器主动发送信息给客户端。
 *
 * WebSocket 特点：
 * 1、建立在TCP协议之上
 * 2、性能开销下通信高效
 * 3、客户端可以与任意服务器通信
 * 4、协议标识符ws wss
 * 5、持久化网络通信协议
 */

use Swoole\WebSocket\Server;

$server = new Server("0.0.0.0", 9501);

// 连接并完成握手后会回调此函数
$server->on('open', function (Swoole\WebSocket\Server $server, $request) {
    echo "server: handshake success with fd {$request->fd}\n";
});

// 必选 监听ws消息事件
$server->on('message', function (Swoole\WebSocket\Server $server, $frame) {
    echo "receive from {$frame->fd}:{$frame->data},opcode:{$frame->opcode},fin:{$frame->finish}\n";
    $server->push($frame->fd, "this is server");
});

$server->on('close', function ($ser, $fd) {
    echo "client {$fd} closed\n";
});

$server->start();
