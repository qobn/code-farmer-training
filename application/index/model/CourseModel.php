<?php
namespace app\index\model;
use think\Model;

class CourseModel extends Model
{
    protected $table = 'course';
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = 'delete_time';
}