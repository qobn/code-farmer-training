<?php
namespace app\index\model;
use think\Model;

class IntentionModel extends Model
{
    protected $table = 'intention';
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = 'delete_time';
}