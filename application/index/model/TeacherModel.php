<?php


namespace app\index\model;


use think\Model;

class TeacherModel extends Model
{
    protected $table = 'teacher';
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = 'delete_time';
}