<?php


namespace app\index\model;


use think\Model;

/**
 * 责任与义务
 * Class ObligationModel
 * @package app\index\model
 */
class ObligationModel extends Model
{
    protected $table = 'obligation';
    protected $pk = 'id';
//    protected $autoWriteTimestamp = true;
//    protected $createTime = 'create_time';
//    protected $updateTime = 'update_time';
//    protected $deleteTime = 'delete_time';
}