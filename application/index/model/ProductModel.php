<?php


namespace app\index\model;


use think\Model;
class ProductModel extends Model
{
    protected $table = 'product';
    protected $pk = 'id';
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = 'delete_time';
}