<?php

namespace app\index\controller;

use app\Components\EmptyData;
use app\Components\ErrorCode;
use app\index\model\IntentionModel;
use app\index\model\ObligationModel;
use app\index\model\ProductModel;
use app\index\model\TeacherModel;
use app\index\model\CourseModel;
use app\index\model\CourseChapterModel;
use think\Controller;
use think\facade\Request;

class Index extends Controller
{
    public function index()
    {
        $resTeacher = TeacherModel::where('id','>','0')->select();
        $productModel = ProductModel::where('id','>','0')->order('sort','asc')->select();
        $intentionModel = IntentionModel::where('id','>','0')->limit(5)->order('create_time','desc')->select();
        $obligationModel = ObligationModel::where('id','>','0')->where('is_del','<>','1')->select();
        $resType1 = [];
        $resType2 = [];
        if (!EmptyData::emptyArray($productModel)){
            for ($i=0;$i<count($productModel);$i++){
                if ($productModel[$i]->type==1){
                    array_push($resType1,$productModel[$i]);
                }else if ($productModel[$i]->type==2){
                    array_push($resType2,$productModel[$i]);
                }
            }
        }

        $resZeRen = [];
        $resQuestion = [];
        if (!EmptyData::emptyArray($obligationModel)){
            for ($i=0;$i<count($obligationModel);$i++){
                if ($obligationModel[$i]->type==0){
                    array_push($resZeRen,$obligationModel[$i]);
                }else if ($obligationModel[$i]->type==1){
                    array_push($resQuestion,$obligationModel[$i]);
                }
            }
        }



        $this->assign('resType1',$resType1);
        $this->assign('resType2',$resType2);
        $this->assign('resTeacher',$resTeacher);
        $this->assign('resIntention',$intentionModel);
        $this->assign('resZeRen',$resZeRen);
        $this->assign('resQuestion',$resQuestion);
        return $this->view->fetch();
    }

    public function hello($name = 'ThinkPHP5')
    {
        return 'hello,' . $name;
    }
    public function video(){
        $productModel = ProductModel::where('id','>','0')->order('sort','asc')->select();
        $this->assign('videos',$productModel);
        return $this->view->fetch();
    }
    public function detail(){
        $type = request()->param('type');
        $course = CourseModel::whereLike('type',$type)->find();  
        // dump(CourseModel::getLastSql());
        $courseChapter = CourseChapterModel::where('course_type',$course->type)->order('id','asc')->select();
        $this->assign('title',$course->name);
        $this->assign('courseChapter',$courseChapter);

        return $this->view->fetch();
    }
    public function teacher(){
        $name = request()->param('name');
        $this->assign("name",$name);
        return $this->view->fetch();
    }

    /**
     * 工单咨询
     */
    public function advisory(){
        $intentionModel = IntentionModel::where('id','>','0')->limit(5)->order('create_time','desc')->select();
        $this->assign('resIntention',$intentionModel);
        return $this->view->fetch();
    }
    public function content()
    {
        return $this->view->fetch();
    }

    /**
     * 意向
     */
    public function intention()
    {
        $courses = array("android", "flutter", "php", "其他");
        $username = Request::param("username");
        $userphone = Request::param("userphone");
        $userqq = Request::param("userqq");
        $course = Request::param("course");
        $desc = Request::param("desc");
        $intentionmodel = new IntentionModel();
        $intentionmodel->username = $username;
        $intentionmodel->userphone = $userphone;
        $intentionmodel->userqq = $userqq;
        $intentionmodel->course = $courses[$course];
        $intentionmodel->desc = $desc;
        $id = $intentionmodel->save();
        if (EmptyData::emptyData($id)){
            return ["code"=>ErrorCode::code_1,"msg"=>"提交失败,请重试"];
        }
        return ["code"=>ErrorCode::code_0,"msg"=>"提交成功,请等候客服与你联系或者可直接从首页联系客服"];
    }

    /**
     * 讲师详情
     */
    public function teacherdetail(){

    }
}
